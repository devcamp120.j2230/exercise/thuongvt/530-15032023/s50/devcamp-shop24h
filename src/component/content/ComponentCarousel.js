import React, { useState } from 'react';
import Carousel from 'react-bootstrap/Carousel';

import imgCarousel1 from "../../assetment/image/JBL Endurance Sprint-products-8.jpg";
import imgCarousel2 from "../../assetment/image/JBL JR 310T pink-products-3.jpg";
import imgCarousel3 from "../../assetment/image/JBL UA Project Rock-products-7 .jpg";

function ComponentCarousel() {
  const [index, setIndex] = useState(0);

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  return (
    <Carousel activeIndex={index} onSelect={handleSelect} style={{paddingLeft: "100px", marginTop: "20px"}}>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={imgCarousel1}
          alt="First slide"
          style={{height:"700px"}}
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={imgCarousel2}
          alt="Second slide"
          style={{height:"700px"}}
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={imgCarousel3}
          alt="Third slide"
          style={{height:"700px"}}
        />
      </Carousel.Item>
    </Carousel>
  );
}

export default ComponentCarousel